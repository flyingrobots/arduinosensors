/* Based on Ping))) Sensor Example by David A. Mellis and by Tom Igoe
 * Reads sonic sensors for arduino to and converts to javascript to be read by ar.drone
 */

// Struct to hold basic information about the sensors
typedef struct HCSR_PINS_STRUCT {
  int trig;
  int echo;
} 
hcsr_pins;

const int SENSOR_COUNT = 5; // Number of Sensors

//specifies the digital i/o pins that sensors are connected to
const hcsr_pins hcsr_pins_list[SENSOR_COUNT] = {
  {3,2},
  {5,4},
  {7,6},
  {9,8},
  {11,10}
};

// minimum distance in centimeters considered "safe"
// If any sensorsr read under this distance the warn LED
// will be lit and the safe turned off. 
const int WARN_LIMIT = 60; 

// the pin of the warning LED and the safe LED
const int warningLED = 13;

long cm[SENSOR_COUNT] = {0,0,0,0,0};

//Variable used to trigger sudo realtime sensor readings
unsigned long time;

void setup() {
  // initialize serial communication:
  Serial.begin(2000000);

  //initialize Pins
  for(int i = 0;i<SENSOR_COUNT;i++)
  {
    InitPins(hcsr_pins_list[i].trig, hcsr_pins_list[i].echo);  
  }

  //initialize time to current time
  time = millis();
}

const int x = 2;

void loop()
{
  //wait 40ms the maximum sensor read time between sensor reads for consistent sensor feed back
  if(millis() - time > 6*5)
  {
    time = millis();

    // read sensor data, and apply low pass filter
    int rd;
    for(int i = 0;i<SENSOR_COUNT;i++)
    {
      rd = readDistance(hcsr_pins_list[i].trig, hcsr_pins_list[i].echo);
      if(rd==0) rd= 123;
      cm[i] = (x * cm[i] + rd) / (x+1);
    }
    //check if any of the sensors are reading less than the warn_limit
    // number of centimeters if so, turn on the warn led and off the safe.
    if (cm[1] < WARN_LIMIT) 
    {
      digitalWrite(warningLED, HIGH);
    } 
    else 
    {
      digitalWrite(warningLED, LOW);
    }

    // print out the sensor data in json format
    Serial.print("{");
    for(int i = 0;i<SENSOR_COUNT;i++)
    {
      Serial.print("\"ping");
      Serial.print(i);
      Serial.print("\": ");
      Serial.print(cm[i]);
      // don't print a comma after the last sensor. 
      if (i != SENSOR_COUNT - 1) { 
        Serial.print(",");  
      }
    }
    
    Serial.print("} ");
    Serial.println();
  }
}

// initialize pins
int InitPins(int trigPin, int echoPin)
{
  pinMode(trigPin, OUTPUT);
  digitalWrite(trigPin, LOW);

  pinMode(echoPin, INPUT);
}

int readDistance(int pingPin, int listenPin) {
  // establish variables for duration of the ping
  long duration;

  // The sensor is triggered by a HIGH pulse of 10 or more microseconds.
  // Give a short LOW pulse beforehand to ensure a clean HIGH pulse:
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(11);
  digitalWrite(pingPin, LOW);

  // Read the signal from the sensor: a HIGH pulse whose
  // duration is the time (in microseconds) from the sending
  // of the ping to the reception of its echo off of an object.
  duration = pulseIn(listenPin, HIGH, 5800);
  delayMicroseconds(1000);

  // convert the time into a distance in centimeters
  return microsecondsToCentimeters(duration);
}

long microsecondsToCentimeters(long microseconds)
{
  // The speed of sound is 340 m/s or 29 microseconds per centimeter.
  // The ping travels out and back, so to find the distance of the
  // object we take half of the distance travelled.
  return microseconds / 29 / 2;
}

